<?php
// This file goes ONE directory up from wherever Wordpress is installed on the Server (or in /source/config.php locally)
$table_prefix = 'wg20200429_';
$url = 'http://knowvid19.vm'; // make sure to set to https on staging/production
define( 'WPMDB_LICENCE', '5b034c85-1773-4c4c-97c4-1773abff475b' );

define( 'DB_NAME', 'wordpress' );
define( 'DB_USER', 'wordpress' );
define( 'DB_PASSWORD', 'wordpress' );
define( 'DB_HOST', 'db:3306' ); // '127.0.0.1', 'localhost', '127.0.0.1:5555' Local (with SSH Tunnel to Staging)
define( 'WP_SITEURL', $url ); 
define( 'WP_HOME', $url );
define( 'FORCE_SSL_LOGIN', false ); // turn on for staging/production
define( 'FORCE_SSL_ADMIN', false ); // turn on for staging/production

// OPTIONS
define( 'DISALLOW_FILE_EDIT', true );
define( 'DB_CHARSET', 'utf8' );
define( 'DB_COLLATE', '' );
define( 'WP_MEMORY_LIMIT', '256M');
define( 'WP_POST_REVISIONS', 3 );

// DEBUG
// https://codex.wordpress.org/Debugging_in_WordPress
define( 'WP_DEBUG', false );
define( 'WP_DEBUG_LOG', false );
define( 'WP_DEBUG_DISPLAY', false );
// @ini_set( 'display_errors', 0 );

// SALT
// https://api.wordpress.org/secret-key/1.1/salt/
define('AUTH_KEY',         '|#.NQM(x:;Bgu%PPo7F:-g}.v)Vh:h5hCsdk):)qz@E)g2RQ{;?IhA=~)NKzpz0*');
define('SECURE_AUTH_KEY',  '8srR}*Zg>A?M*R|7I(NPSg&(d-v]R.M}./LDq^5q|M{r(8-]Ce:F:rDEf|!NkD{ ');
define('LOGGED_IN_KEY',    '=y(?+FPY JHa[Q@cR,+^.9pHg^cF%|v#WP@jG{Z|NTD%)#5#G#Tfp$LTjc%g7)Ik');
define('NONCE_KEY',        'jA#b)>|Jy=D-{6G]?Af?AztN>b[gV(_B+@-$ppV@ZM3SsvCUIV_w&Ne`@~Az+|fq');
define('AUTH_SALT',        '+H<an,pyO(MDvlc]%`r$y_I#KTJSCFrrc4f (RG,dqIE6-?HA:D,wW+J3)O1[rE0');
define('SECURE_AUTH_SALT', 'kh<Zs72v-VDk[2-)1Q+N_STzyjiXlap< (WEE91]x?Sx|I:w&zXsm@RA{K_IoI~%');
define('LOGGED_IN_SALT',   'py[!r<+|aQG!-WTX]Wp- K.qytVexwgtPXuE:sG@MZnqV|iN]JgWJNeEPfNXFssO');
define('NONCE_SALT',       'ZSioLk,kPi@~}~A^3m#!)iGDmN7Yn-gED}atc}<8$$FJK8FWL?o7N;@R>|x@*s1A');