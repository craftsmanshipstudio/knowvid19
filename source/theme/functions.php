<?php
/**
 * Functions and Definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package custom-theme
 */

// --------------------------------------------------------------------------------
// REMOVE STUFF

remove_action('wp_head', 'wp_generator'); // Version
remove_action('welcome_panel', 'wp_welcome_panel'); // Welcome Message
add_filter('the_generator', '__return_empty_string'); // Version in RSS
add_filter('show_admin_bar', '__return_false'); // Admin Bar
add_filter('xmlrpc_enabled', '__return_false'); // Disable XML-RPC
add_filter('types_information_table', '__return_false'); // Remove 'Types' Page menu

// REMOVE WP VERSION FROM STYLES & SCRIPTS
function shapeSpace_remove_version_scripts_styles($src) {
	if (strpos($src, 'ver=')) {
		$src = remove_query_arg('ver', $src);
	}
	return $src;
}
add_filter('style_loader_src', 'shapeSpace_remove_version_scripts_styles', 9999);
add_filter('script_loader_src', 'shapeSpace_remove_version_scripts_styles', 9999);

// REMOVE DASHICONS
add_action( 'wp_print_styles', 'my_deregister_styles', 100 );
function my_deregister_styles() {
  wp_deregister_style( 'dashicons' );
}

// REMOVE ADMIN BAR
add_action('get_header', 'remove_admin_login_header');
function remove_admin_login_header() {
	remove_action('wp_head', '_admin_bar_bump_cb');
}

// --------------------------------------------------------------------------------
// ADD STUFF

if ( function_exists( 'add_theme_support' ) ) {
  add_theme_support( 'menus' ); // ADD MENU SUPPORT
  add_theme_support( 'automatic-feed-links' ); // Add default posts and comments RSS feed links to head.
  add_theme_support( 'title-tag' ); // Title Tag Support
  add_theme_support( 'post-thumbnails' ); // Post Thumbnails
  add_theme_support( 'customize-selective-refresh-widgets' ); // Add theme support for selective refresh for widgets.
}

// ADD ACF OPTIONS
if( function_exists('acf_add_options_page') ) {
  acf_add_options_page();
  // acf_add_options_sub_page('Shared Options');
}

// --------------------------------------------------------------------------------
// CUSTOMIZE STUFF

// Login Logo URL
function my_login_logo_url() {
  return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );
function my_login_logo_url_title() {
    return 'knowvid19';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

// Login Styles
function my_login_stylesheet() {
  wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/style-login.css' );
  //wp_enqueue_script( 'custom-login', get_stylesheet_directory_uri() . '/style-login.js' );
}
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );

// CUSTOM IMAGE SIZES
// add_image_size( 'blog-thumb', 626, 352, true );

// --------------------------------------------------------------------------------
// OTHER STUFF

// Redirect logins to wp-migrate on localhost
// if ($_SERVER['HTTP_HOST'] == 'knowvid19.vm') {
//   function my_login_redirect( $url, $request, $user ){
//     if( $user && is_object( $user ) && is_a( $user, 'WP_User' ) ) {
//       if( $user->has_cap( 'administrator') or $user->has_cap( 'author')) {
//         $url = admin_url('/tools.php?page=wp-sync-db');
//       } else {
//         $url = home_url();
//       }
//     }
//     return $url;
//   }
//   add_filter('login_redirect', 'my_login_redirect', 10, 3 );
// }

// Enqueue scripts and styles.
function custom_scripts() {
	wp_enqueue_style('custom-style', get_stylesheet_uri());
  wp_enqueue_style('custom-google-fonts', 'https://fonts.googleapis.com/css?family=Montserrat:300,600|Roboto:300', false);
  // wp_enqueue_style('font-awesome', 'https://use.fontawesome.com/releases/v5.7.2/css/all.css', false);
	wp_enqueue_script('jquery', '//code.jquery.com/jquery-3.2.1.min.js', array(), '', true);
	wp_enqueue_script('custom-script-main', get_template_directory_uri() . '/js/app.js', array('jquery'), '', true);

  // this trick is used to pass the template URL into custom-main-scripts. It can be refered to using: site.theme_path
  // wp_localize_script("custom-script-main", "site", array( "theme_path" => get_template_directory_uri() ) );
}
add_action( 'wp_enqueue_scripts', 'custom_scripts' );

// Switch default core markup for search form, comment form, and comments to output valid HTML5.
add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

// --------------------------------------------------------------------------------
// ANGRY MESSAGE FOR DEVELOPERS

function showAdminMessage()
{
  if ($_SERVER['HTTP_HOST'] == 'knowvid19.vm') {
    echo
    '<div style="background-color:red; padding:20px; margin-top: 10px; margin-right:20px; font-weight: bold;  color: white;">' .
      'DEVELOPERS: <br>'.
      'Tools > Migrate DB to pull down the latest from Staging.<br>' .
      '-----------------------------------------------------------------<br>' .
      'IF YOU MAKE CHANGES LOCALLY THEY WILL BE OVERWRITTEN.<br>' .
      'All other changes (including uploading media) should be made on Staging, and pulled to Local. NO LOCAL DATABASE CHANGES.<br>' .
    '</div>';
  } else {
    echo
    '<div style="background-color:green; padding:20px; margin-top: 10px; margin-right:20px; font-weight: bold; color: white;">' .
       'DEVELOPER: You appear to be on Staging or Production. Feel free to make changes here to pull locally.' .
    '</div>';
  }
}
add_action('admin_notices', 'showAdminMessage');

// --------------------------------------------------------------------------------
// OUR FUNCTIONS
require_once( __DIR__ . '/functions/featured-image.php');

// --------------------------------------------------------------------------------
// CUSTOM FOR CLIENT
