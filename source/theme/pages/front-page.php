<?php
/* Template Name: Front Page */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package custom-theme
 */

get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>

  <?php //get_template_part( 'partials/header', 'nav' ); ?>

  <div class="p-5 d-flex justify-content-center align-items-center" class="logo" style="height: 100vh">
    <img src="/wp-content/themes/knowvid19/images/logos/logo-knowvid19.svg" alt="Knowvid19" class="logo-img">
  </div>

<?php endwhile; else: endif; ?>
<?php get_footer();
